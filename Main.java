package parser;

import java.io.IOException;
import org.apache.hc.client5.http.fluent.Content;
import org.apache.hc.client5.http.fluent.Request;

public class Main {
    public static void main(String[] args) throws IOException {
        Content content = Request.post("https://www.opennet.ru/opennews/mini.shtml").execute().returnContent();
        String contentToString = content.toString();
        String[] words = contentToString.split(" ");
        int count = 0;
        for (String word : words) {
            if (word.contains("31.08.2021")) {
                count++;
            }
        }
        System.out.println(count);
    }
}



